CREATE TABLE tx_products_domain_model_product (
    title VARCHAR(255) DEFAULT '' NOT NULL,
    slug VARCHAR(255) DEFAULT '' NOT NULL,

    price DECIMAL(10,2) DEFAULT 0.0 NOT NULL,
    sku INT(11) DEFAULT 0 NOT NULL,

    categories INT(11) DEFAULT 0 NOT NULL,
    media INT(11) DEFAULT 0 NOT NULL,
    variations INT(11) DEFAULT 0 NOT NULL,
) Engine=InnoDB;

CREATE TABLE tx_products_domain_model_product_variation (
    title VARCHAR(255) DEFAULT '' NOT NULL,
    color VARCHAR(255) DEFAULT '' NOT NULL,

    product INT(11) DEFAULT 0 NOT NULL,
) Engine=InnoDB;

CREATE TABLE tt_content (
    tx_products_product_spotlight INT(11) DEFAULT 0 NOT NULL,
    tx_products_inline_items INT(11) DEFAULT 0 NOT NULL,
) Engine=InnoDB;