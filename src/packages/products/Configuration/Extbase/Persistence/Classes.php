<?php

return [
    \Kurs\Products\Domain\Model\Category::class => [
        'tableName' => 'sys_category',
    ],
];