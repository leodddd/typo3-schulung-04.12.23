<?php

return [
    'ctrl' => [
        'title' => 'Variation',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'typeicon_classes' => [
            'default' => 'content-timeline'
        ],
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
        'hideTable' => true,
    ],
    'columns' => [
        'title' => [
            'label' => 'Titel',
            'config' => [
                'type' => 'input',
                'eval' => 'trim'
            ],
        ],
        'color' => [
            'label' => 'Farbe',
            'config' => [
                'type' => 'color',
            ],
        ],
        'product' => [
            'label' => 'Produkt',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_products_domain_model_product',
            ],
        ],
    ],
    'palettes' => [
    ],
    'types' => [
        '0' => [
            'showitem' => '
                title,
                color,
            ',
        ],
    ]
];
