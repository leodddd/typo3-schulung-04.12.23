<?php

return [
    'ctrl' => [
        'title' => 'Product',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'typeicon_classes' => [
            'default' => 'content-briefcase'
        ],
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
    ],
    'columns' => [
        'title' => [
            'label' => 'Titel',
            'config' => [
                'type' => 'input',
                'eval' => 'trim'
            ],
        ],
        'media' => [
            'label' => 'Medien',
            'config' => [
                'type' => 'file',
            ],
        ],
        'slug' => [
            'label' => 'URL-Segment',
            'config' => [
                'type' => 'slug',
                'generatorOptions' => [
                    'fields' => [
                        [
                            'title',
                            'sku'
                        ],
                    ],
                    'fieldSeparator' => '/',
                ],
                'fallbackCharacter' => '-',
                'eval' => 'uniqueInSite',
            ],
        ],
        'price' => [
            'label' => 'Preis',
            'config' => [
                'type' => 'number',
                'format' => 'decimal'
            ],
        ],
        'sku' => [
            'label' => 'SKU',
            'config' => [
                'type' => 'number'
            ],
        ],
        'categories' => [
            'label' => 'Kategorien',
            'config' => [
                'type' => 'category'
            ],
        ],
        'variations' => [
            'label' => 'Variationen',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_products_domain_model_product_variation',
                'foreign_field' => 'product',
            ]
        ]
    ],
    'palettes' => [
        'titleSlug' => [
            'showitem' => '
                title,
                --linebreak--,
                slug,
            ',
        ]
    ],
    'types' => [
        '0' => [
            'showitem' => '
                --palette--;Names;titleSlug,
                price,
                sku,
                variations,

                --div--;Medien,
                media,

                --div--;Kategorien,
                categories,
            ',
        ],
    ]
];
