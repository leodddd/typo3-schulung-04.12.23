<?php

$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'products',
    'productList',
    'Products: List'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:products/Configuration/FlexForms/ProductList.xml'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'label' => 'Product Spotlight',
        'value' => 'products_productspotlight',
        'icon' => 'content-briefcase',
        'group' => 'products',
        'description' => 'Product Spotlight',
    ],
    'textmedia',
    'after'
);

$GLOBALS['TCA']['tt_content']['columns']['tx_products_product_spotlight'] = [
    'label' => 'Product Spotlight',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'foreign_table' => 'tx_products_domain_model_product',
        'items' => [
            [
                'label' => '-------',
                'value' => 0,
            ],
        ],
    ],
];
$GLOBALS['TCA']['tt_content']['columns']['tx_products_inline_items'] = [
    'label' => 'Inline items',
    'config' => [
        'type' => 'inline',
    ],
];


$GLOBALS['TCA']['tt_content']['types']['products_productspotlight'] = [
    'showitem' => '
        --palette--;;general,
        --palette--;;headers,
        bodytext,
        tx_products_product_spotlight,
        
    ',
];