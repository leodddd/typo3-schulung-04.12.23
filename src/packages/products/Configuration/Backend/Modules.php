<?php

use Kurs\Products\Controller\Backend\ProductImportController;

return [
    ProductImportController::class => [
        'parent' => 'web',
        'position' => [
            'after' => 'web_info',
        ],
        'access' => 'user',
        'path' => '/module/products/import',
        'extensionName' => 'Products',
        'labels' => [
            'title' => 'Import products',
            'description' => 'Imports products from a given REST API blah blah',
            'shortDescription' => 'Imports products from a given REST API',
        ],
        'iconIdentifier' => 'module-documentation',
        'controllerActions' => [
            ProductImportController::class => 'form, import',
        ],
    ],
];