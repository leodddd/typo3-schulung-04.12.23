<?php

return [
    'frontend' => [
        \Kurs\Products\Middleware\ProductApiMiddleware::class => [
            'target' => \Kurs\Products\Middleware\ProductApiMiddleware::class,
            'before' => [
                'typo3/cms-frontend/page-argument-validator',
                'typo3/cms-frontend/page-resolver',
            ],
            'after' => [
                'typo3/cms-frontend/site',
            ],
        ]
    ],
];