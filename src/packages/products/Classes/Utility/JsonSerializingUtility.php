<?php

namespace Kurs\Products\Utility;

use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class JsonSerializingUtility
{
    /** @param ObjectStorage<FileReference> $fileReferences */
    public static function serializeFileReferences(ObjectStorage $fileReferences): array
    {
        return array_map(
            function (FileReference $fileReference): array {
                return [
                    'url' => $fileReference->getOriginalResource()->getPublicUrl(),
                    'filename' => $fileReference->getOriginalResource()->getOriginalFile()->getName(),
                ];
            },
            $fileReferences->toArray()
        );
    }
}