<?php

namespace Kurs\Products\Utility;

use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SiteConfigurationUtility
{
    public static function getProductsEndpoint(?Site $site = null): string
    {
        return static::getSiteConfiguration($site)['endpoints']['product'] ?? '';
    }

    public static function isSiteConfigured(?Site $site = null): bool
    {
        return ($site->getConfiguration()['txProducts'] ?? null) !== null;
    }
    public static function getSiteConfiguration(?Site $site = null): array
    {
        $site = $site ?? static::getDefaultSite();

        return $site->getConfiguration()['txProducts'] ?? [];
    }
    public static function getDefaultSite(): Site
    {
        return GeneralUtility::makeInstance(SiteFinder::class)->getAllSites()[0];
    }
}
