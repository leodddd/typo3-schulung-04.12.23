<?php

namespace Kurs\Products\EventListener;

use TYPO3\CMS\Core\Resource\Event\BeforeFileRenamedEvent;

class BeforeFileRenameEventListener
{
    public function __invoke(
        BeforeFileRenamedEvent $event
    ): void {
        // React to file renaming here.

        return;
    }
}