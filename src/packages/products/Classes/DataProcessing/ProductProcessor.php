<?php

namespace Kurs\Products\DataProcessing;

use Kurs\Products\Domain\Repository\ProductRepository;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

class ProductProcessor implements DataProcessorInterface
{
    public function __construct(
        protected ProductRepository $productRepository,
    ) {
        $this->productRepository->setDefaultQuerySettings(
            $this->productRepository->createQuery()->getQuerySettings()
                ->setRespectStoragePage(false)
        );
    }

    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ): array {
        $productUid = $processedData['data']['tx_products_product_spotlight'] ?? null;
        if ($productUid === null) {
            return $processedData;
        }

        $product = $this->productRepository->findByUid($productUid);
        if ($product === null) {
            return $processedData;
        }

        $as = $processorConfiguration['as'] ?? 'product';
        $processedData[$as] = $product;

        return $processedData;
    }
}