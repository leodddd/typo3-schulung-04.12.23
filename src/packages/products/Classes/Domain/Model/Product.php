<?php

namespace Kurs\Products\Domain\Model;

use JsonSerializable;
use Kurs\Products\Domain\Model\Product\Variation;
use Kurs\Products\Utility\JsonSerializingUtility;
use TYPO3\CMS\Extbase\Annotation\Validate;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Product extends AbstractEntity implements JsonSerializable
{
    #[Validate([
        'validator' => 'StringLength',
        'options' => [
            'minimum' => 5
        ]
    ])]
    protected string $title = '';
    protected string $slug = '';
    protected float $price = .0;
    protected int $sku = 0;

    /** @var ObjectStorage<Category> $categories */
    protected ObjectStorage $categories;
    /** @var ObjectStorage<FileReference> $media */
    protected ObjectStorage $media;
    /** @var ObjectStorage<Variation> $variations */
    protected ObjectStorage $variations;

    public function __construct()
    {
        $this->initializeObject();
    }

    public function initializeObject(): void
    {
        $this->categories = new ObjectStorage();
        $this->media = new ObjectStorage();
    }

    public function jsonSerialize(): array
    {
        return [
            'uid' => $this->getUid(),
            'title' => $this->getTitle(),
            'slug' => $this->getSlug(),
            'price' => $this->getPrice(),
            'sku' => $this->getSku(),
            'categories' => $this->getCategories()->toArray(),
            'media' => JsonSerializingUtility::serializeFileReferences($this->getMedia()),
        ];
    }

    // Nicht der richtige Weg, um 'eval' des TCA-Felds
    // zu berücksichtigen muss der SlugHelper verwendet werden.
    public function updateSlug(): self
    {
        $this->setSlug(
            strtolower(
                str_replace([' ', '.', '\'', '&'], '-', $this->getTitle())
            )
        );

        return $this;
    }

    /**
     * Get the value of title
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * Set the value of slug
     *
     * @param string $slug
     *
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get the value of price
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Set the value of price
     *
     * @param float $price
     *
     * @return self
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get the value of sku
     *
     * @return int
     */
    public function getSku(): int
    {
        return $this->sku;
    }

    /**
     * Set the value of sku
     *
     * @param int $sku
     *
     * @return self
     */
    public function setSku(int $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get the value of categories
     *
     * @return ObjectStorage
     */
    public function getCategories(): ObjectStorage
    {
        return $this->categories;
    }

    /**
     * Set the value of categories
     *
     * @param ObjectStorage $categories
     *
     * @return self
     */
    public function setCategories(ObjectStorage $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Get the value of media
     *
     * @return ObjectStorage
     */
    public function getMedia(): ObjectStorage
    {
        return $this->media;
    }

    /**
     * Set the value of media
     *
     * @param ObjectStorage $media
     *
     * @return self
     */
    public function setMedia(ObjectStorage $media): self
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get the value of variations
     *
     * @return ObjectStorage
     */
    public function getVariations(): ObjectStorage
    {
        return $this->variations;
    }

    /**
     * Set the value of variations
     *
     * @param ObjectStorage $variations
     *
     * @return self
     */
    public function setVariations(ObjectStorage $variations): self
    {
        $this->variations = $variations;

        return $this;
    }
}