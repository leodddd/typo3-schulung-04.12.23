<?php

namespace Kurs\Products\Domain\Model;

use JsonSerializable;
use TYPO3\CMS\Extbase\Domain\Model\Category as ExtbaseCategory;

class Category extends ExtbaseCategory implements JsonSerializable
{
    public function jsonSerialize(): array
    {
        return [
            'title' => $this->getTitle(),
        ];
    }
}