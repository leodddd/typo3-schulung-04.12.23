<?php

namespace Kurs\Products\Domain\Model\Product;

use Exception;
use Kurs\Products\Domain\Model\Product;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class Variation extends AbstractEntity
{
    protected string $title = '';
    protected string $color = '';
    protected ?Product $product = null;

    /**
     * Get the value of title
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of color
     *
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * Set the value of color
     *
     * @param string $color
     *
     * @return self
     */
    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get the value of product
     *
     * @return Product
     */
    public function getProduct(): Product
    {
        if ($this->product === null) {
            throw new Exception('Product not found.');
        }
        return $this->product;
    }

    /**
     * Set the value of product
     *
     * @param Product $product
     *
     * @return self
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}