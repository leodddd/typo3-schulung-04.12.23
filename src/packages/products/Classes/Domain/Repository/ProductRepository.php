<?php

namespace Kurs\Products\Domain\Repository;

use Kurs\Products\Domain\Model\Category;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

class ProductRepository extends Repository
{
    public function findByCategory(Category $category): QueryResultInterface
    {
        $query = $this->createQuery();

        $query = $query->matching(
            $query->contains('categories', $category)
        );

        return $query->execute();
    }
}