<?php

namespace Kurs\Products\Import;

use Exception;
use Kurs\Products\Domain\Model\Category;
use Kurs\Products\Domain\Model\Product;
use Kurs\Products\Domain\Repository\CategoryRepository;
use Kurs\Products\Domain\Repository\ProductRepository;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Http\RequestFactory;
use TYPO3\CMS\Core\Resource\FileReference as CoreFileReference;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Extbase\Domain\Model\FileReference as ExtbaseFileReference;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class ProductHydrator
{
    public function __construct(
        protected ProductRepository $productRepository,
        protected CategoryRepository $categoryRepository,
        protected PersistenceManager $persistenceManager,
        protected RequestFactory $requestFactory,
        protected Environment $environment,
        protected ResourceFactory $resourceFactory,
    ) {
        $this->productRepository->setDefaultQuerySettings(
            $this->productRepository->createQuery()->getQuerySettings()
                ->setRespectStoragePage(false)
        );
        $this->categoryRepository->setDefaultQuerySettings(
            $this->categoryRepository->createQuery()->getQuerySettings()
                ->setRespectStoragePage(false)
        );
    }

    public function hydrateFromDummyJson(array $productData, int $pageUid): array
    {
        $hydratedProducts = array_map(
            function (array $singleProductData) use ($pageUid): ?Product {
                try {
                    return $this->hydrateProductFromDummyJson($singleProductData, $pageUid);
                }
                catch (ImportException $e) {
                    return null;
                }
            },
            $productData['products'] ?? []
        );

        return array_filter(
            $hydratedProducts,
            function (?Product $product): bool {
                return $product !== null;
            }
        );
    }

    protected function hydrateProductFromDummyJson(array $singleProductData, int $pageUid): Product
    {
        $dummyJsonId = $singleProductData['id'] ?? 0;

        /** @var Product $product */
        $product = $this->productRepository->findOneBy([
            'sku' => $dummyJsonId
        ]) ?? new Product();

        // Alternativ: config.tx_products.persistence.storagePid = <pageUid>
        $product->setPid($pageUid);
        $product
            ->setTitle($singleProductData['title'] ?? '')
            ->setPrice($singleProductData['price'] ?? .0)
            ->setSku($dummyJsonId)
            ->setCategories($this->hydrateCategoryFromDummyJson($singleProductData['category'] ?? null, $pageUid))
        ;

        $product->updateSlug();

        $product->setMedia(
            $this->replaceDuplicateFileReferences(
                $this->hydrateFileReferencesFromDummyJson($singleProductData['images'] ?? [], $product),
                $product->getMedia()
            )
        );

        return $product;
    }

    /** @return ObjectStorage<Category> */
    protected function hydrateCategoryFromDummyJson(?string $categoryName, int $pageUid): ObjectStorage
    {
        $categories = new ObjectStorage();

        if ($categoryName === null || $categoryName === '') {
            return $categories;
        }

        /** @var Category $category */
        $category = $this->categoryRepository->findOneBy([
            'title' => $categoryName
        ]) ?? new Category();
        
        $category->setPid($pageUid);
        $category->setTitle($categoryName);

        $category->_isNew()
            ? $this->categoryRepository->add($category)
            : $this->categoryRepository->update($category)
        ;
        $this->persistenceManager->persistAll();

        $categories->attach($category);
        return $categories;
    }

    /** @return ObjectStorage<ExtbaseFileReference> */
    protected function hydrateFileReferencesFromDummyJson(array $fileReferencesData, Product $product): ObjectStorage
    {
        $fileReferences = new ObjectStorage();

        foreach ($fileReferencesData as $fileUrl) {
            $fileReferences->attach($this->downloadFileReference($fileUrl, $product));
        }

        return $fileReferences;
    }

    protected function downloadFileReference(string $url, Product $product): ExtbaseFileReference
    {
        $baseFileName = basename($url);
        $localFilePath = 'user_upload/' . $product->getSlug() . '/' . $baseFileName;
        try {
            $falFile = $this->resourceFactory->getFileObjectFromCombinedIdentifier(
                '1:' . $localFilePath
            );
        }
        catch (Exception $e) {
            $falFile = null;
        }

        if ($falFile === null) {
            $tempPath = $this->environment->getVarPath() . '/tmp/' . $baseFileName;
            $this->requestFactory->request($url, 'GET', [
                'sink' => $tempPath
            ]);

            $storage = $this->resourceFactory->getStorageObject(1);
            $folderPath = dirname($localFilePath);
            $folder = $storage->getRootLevelFolder();
            foreach(explode('/', $folderPath) as $subFolderName) {
                if (!$folder->hasFolder($subFolderName)) {
                    $folder->createFolder($subFolderName);
                }
                $folder = $folder->getSubfolder($subFolderName);
            }
            $falFile = $folder->addFile($tempPath, $baseFileName);
        }

        $coreFileReference = new CoreFileReference([
            'uid_local' => $falFile->getUid()
        ]);
        $extbaseFileReference = new ExtbaseFileReference();
        $extbaseFileReference->setOriginalResource($coreFileReference);

        return $extbaseFileReference;
    }

    protected function replaceDuplicateFileReferences(
        ObjectStorage $newReferences,
        ObjectStorage $existingReferences
    ): ObjectStorage {
        $duplicateFreeStorage = new ObjectStorage();

        /** @var ExtbaseFileReference $newReference */
        foreach ($newReferences as $newReference) {
            $referenceToKeep = $newReference;
            /** @var ExtbaseFileReference $existingReference */
            foreach ($existingReferences->toArray() as $existingReference) {
                if (
                    $newReference->getOriginalResource()->getOriginalFile()->getUid()
                    === $existingReference->getOriginalResource()->getOriginalFile()->getUid()
                ) {
                    $referenceToKeep = $existingReference;
                    break;
                }
            }

            $duplicateFreeStorage->attach($referenceToKeep);
        }

        return $duplicateFreeStorage;
    }
}