<?php

namespace Kurs\Products\Import;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Kurs\Products\Domain\Model\Product;
use TYPO3\CMS\Core\Http\Uri;

class ProductImportService
{
    public function __construct(
        protected Client $guzzle,
        protected ProductHydrator $productHydrator,
    ) {}

    /** @return array<Product> */
    public function importFromUrl(string $url, int $pageUid): array
    {
        $endpointUri = (new Uri($url))
            ->withQuery(
                http_build_query([
                    'limit' => 0
                ])
            )
        ;

        try {
            $productResponse = $this->guzzle->get((string)$endpointUri);
        }
        catch (GuzzleException $e) {
            return [];
        }

        $productData = json_decode($productResponse->getBody()->getContents(), true);
        return $this->productHydrator->hydrateFromDummyJson($productData, $pageUid);
    }
}