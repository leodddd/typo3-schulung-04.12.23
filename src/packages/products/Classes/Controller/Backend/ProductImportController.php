<?php

namespace Kurs\Products\Controller\Backend;

use Kurs\Products\Domain\Repository\ProductRepository;
use Kurs\Products\Import\ProductImportService;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Backend\Attribute\Controller;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

#[Controller]
class ProductImportController extends ActionController
{
    public function __construct(
        protected ModuleTemplateFactory $moduleTemplateFactory,
        protected ProductRepository $productRepository,
        protected ProductImportService $productImportService,
        protected PersistenceManager $persistenceManager,
    ) {
        $this->productRepository->setDefaultQuerySettings(
            $this->productRepository->createQuery()->getQuerySettings()
                ->setRespectStoragePage(false)
        );
    }

    public function formAction(): ResponseInterface
    {
        $this->view->assign('products', $this->productRepository->findAll());

        $moduleTemplate = $this->moduleTemplateFactory->create($this->request);
        $moduleTemplate->setContent($this->view->render());

        // Add buttons, dropdowns, etc.
        // $moduleTemplate->getDocHeaderComponent()->getButtonBar()->addButton(...)

        return $this->htmlResponse($moduleTemplate->renderContent());
    }

    public function importAction(int $id = 0, ?string $endpoint = null): ResponseInterface
    {
        if ($endpoint === null) {
            $this->addFlashMessage(
                'Could not import products: missing endpoint.'
            );

            return $this->redirect('form');
        }

        $importedProducts = $this->productImportService->importFromUrl($endpoint, $id);
        foreach ($importedProducts as $importedProduct) {
            $importedProduct->_isNew()
                ? $this->productRepository->add($importedProduct)
                : $this->productRepository->update($importedProduct)
            ;
        }
        $this->persistenceManager->persistAll();

        $this->addFlashMessage(
            'Successfully imported products.'
        );

        return $this->redirect('form');
    }
}