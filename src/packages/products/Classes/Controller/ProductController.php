<?php

namespace Kurs\Products\Controller;

use Kurs\Products\Domain\Model\Category;
use Kurs\Products\Domain\Model\Product;
use Kurs\Products\Domain\Repository\CategoryRepository;
use Kurs\Products\Domain\Repository\ProductRepository;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Extbase\Annotation\IgnoreValidation;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class ProductController extends ActionController
{
    public function __construct(
        protected ProductRepository $productRepository,
        protected CategoryRepository $categoryRepository,
        protected PersistenceManager $persistenceManager,
    ) {}

    public function listAction(?Category $filterByCategory = null): ResponseInterface
    {
        $products = $filterByCategory === null
            ? $this->productRepository->findAll()
            : $this->productRepository->findByCategory($filterByCategory)
        ;

        $this->view->assign('products', $products);
        $this->view->assign('categories', $this->categoryRepository->findAll());

        return $this->htmlResponse();
    }
    
    public function showAction(Product $product): ResponseInterface
    {
        $this->view->assign('product', $product);

        return $this->htmlResponse();
    }

    #[IgnoreValidation(['argumentName' => 'product'])]
    public function editAction(Product $product): ResponseInterface
    {
        $this->view->assign('product', $product);

        return $this->htmlResponse();
    }

    public function updateAction(Product $product): ResponseInterface
    {
        $this->addFlashMessage(
            'Produkt erfolgreich aktualisiert.',
            'Erfolg',
            ContextualFeedbackSeverity::OK
        );

        $product->updateSlug();

        $product->_isNew()
            ? $this->productRepository->add($product)
            : $this->productRepository->update($product)
        ;
        $this->persistenceManager->persistAll();

        return $this->redirect(
            'show',
            null,
            null,
            ['product' => $product]
        );
    }

    public function deleteAction(Product $product): ResponseInterface
    {
        $this->addFlashMessage(
            'Produkt erfolgreich gelöscht.',
            'Erfolg',
            ContextualFeedbackSeverity::OK
        );

        $this->productRepository->remove($product);
        $this->persistenceManager->persistAll();

        return $this->redirect('list');
    }

    public function createAction(?Product $product = null): ResponseInterface
    {
        $this->view->assign('product', $product);
        
        return $this->htmlResponse();
    }
}