<?php

namespace Kurs\Products\Form\Finisher;

use Kurs\Products\Domain\Model\Product;
use Kurs\Products\Domain\Repository\ProductRepository;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Form\Domain\Finishers\AbstractFinisher;

class NewProductFinisher extends AbstractFinisher
{
    public function __construct(
        protected ProductRepository $productRepository,
        protected PersistenceManager $persistenceManager,
    ) {}

    protected function executeInternal(): ?string
    {
        $title = $this->parseOption('title');
        $price = (int)$this->parseOption('price');
        $sku = (int)$this->parseOption('sku');
        $pid = (int)$this->parseOption('pid');

        $newProduct = new Product();
        $newProduct
            ->setTitle($title)
            ->setPrice($price)
            ->setSku($sku)
            ->setPid($pid)
        ;

        $this->productRepository->add($newProduct);
        $this->persistenceManager->persistAll();

        return null;
    }
}