<?php

namespace Kurs\Products\Command;

use Kurs\Products\Domain\Repository\ProductRepository;
use Kurs\Products\Import\ProductImportService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Fluid\View\TemplatePaths;

class ProductImportCommand extends Command
{
    public function __construct(
        protected ProductImportService $productImportService,
        protected ProductRepository $productRepository,
        protected PersistenceManager $persistenceManager,
        protected Mailer $mailer,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption(
            'pageUid',
            'p',
            InputOption::VALUE_REQUIRED,
            'Page UID to import products into.'
        );
        $this->addOption(
            'email',
            'e',
            InputOption::VALUE_REQUIRED,
            'E-Mail to send a summary of the finished import to.'
        );

        $this->addArgument(
            'endpointUrl',
            InputArgument::REQUIRED,
            'REST API Endpoint to import product data from.'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $endpointUrl = $input->getArgument('endpointUrl');

        $pageUid = 1;
        if ($input->hasOption('pageUid')) {
            $pageUid = $input->getOption('pageUid') ?? null;
        }
        if (!MathUtility::canBeInterpretedAsInteger($pageUid)) {
            $pageUid = 1;
        }
        $pageUid = (int)$pageUid;

        $email = null;
        if ($input->hasOption('email')) {
            $email = $input->getOption('email');
        }

        if (!GeneralUtility::isValidUrl($endpointUrl)) {
            $output->writeln('Argument "endpointUrl" is not a valid URL.');
            return 1;
        }

        $output->write('Beginning import from URL: "' . $endpointUrl . '"...');
        $importedProducts = $this->productImportService->importFromUrl($endpointUrl, $pageUid);
        $output->writeln(' Done.');
        $output->write('Persisting imported products...');
        foreach ($importedProducts as $importedProduct) {
            $importedProduct->_isNew()
                ? $this->productRepository->add($importedProduct)
                : $this->productRepository->update($importedProduct)
            ;
        }
        $this->persistenceManager->persistAll();
        $output->writeln(' Done.');

        if ($email !== null) {
            $this->notifyEmail($email, $importedProducts);
        }

        return 0;
    }

    /** @param array<Product> $products */
    protected function notifyEmail(string $email, array $products): void
    {
        $standaloneView = new StandaloneView();
        $standaloneView->setTemplatePathAndFilename('EXT:products/Resources/Private/Templates/Email/ProductImportSummary.html');
        $standaloneView->assign('products', $products);

        $mail = new FluidEmail();
        $mail
            ->to($email)
            ->subject('Product import summary')
            ->setTemplate('ProductImportSummary')
            ->format('html')
            ->assign('products', $products)
            ->attach($standaloneView->render(), 'test.txt')
        ;

        $this->mailer->send($mail);
    }
}