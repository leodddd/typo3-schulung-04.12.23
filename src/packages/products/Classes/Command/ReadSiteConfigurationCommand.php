<?php

namespace Kurs\Products\Command;

use Kurs\Products\Utility\SiteConfigurationUtility;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Site\SiteFinder;

class ReadSiteConfigurationCommand extends Command
{
    public function __construct(
        protected SiteFinder $siteFinder,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->siteFinder->getAllSites() as $site) {
            if (!SiteConfigurationUtility::isSiteConfigured($site)) {
                continue;
            }

            debug(
                SiteConfigurationUtility::getSiteConfiguration($site)
            );
        }

        return 0;
    }
}