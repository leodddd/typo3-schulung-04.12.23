<?php

namespace Kurs\Products\Middleware;

use Kurs\Products\Domain\Repository\ProductRepository;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Http\ResponseFactory;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ProductApiMiddleware implements MiddlewareInterface
{
    const ENDPOINT = 'api/products';

    public function __construct(
        protected ResponseFactory $responseFactory,
    ) {}

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->matches($request)) {
            return $this->handleRequest($request);
        }

        return $handler->handle($request);
    }

    public function matches(ServerRequestInterface $request): bool
    {
        /** @var ?SiteLanguage $siteLanguage */
        $siteLanguage = $request->getAttribute('language');
        if ($siteLanguage === null) {
            return false;
        }

        $languageBase = $siteLanguage->getBase();
        $visitedUrl = $request->getUri();

        if ($languageBase->getHost() !== $visitedUrl->getHost()) {
            return false;
        }

        $visitedPath = trim($visitedUrl->getPath(), '/');
        $languagePath = trim($languageBase->getPath(), '/');

        if (
            !str_starts_with($visitedPath, $languagePath)
            || substr($visitedPath, 0, strlen($visitedPath) - strlen($languagePath)) !== static::ENDPOINT
        ) {
            return false;
        }

        return true;
    }

    public function handleRequest(ServerRequestInterface $request): ResponseInterface
    {
        $productRepository = GeneralUtility::makeInstance(ProductRepository::class);
        $productRepository->setDefaultQuerySettings(
            $productRepository->createQuery()->getQuerySettings()
                ->setRespectStoragePage(false)
        );

        $uidFilter = $request->getQueryParams()['uid'] ?? null;

        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json')
        ;
        $response->getBody()->write(
            json_encode(
                $uidFilter === null
                    ? $productRepository->findAll()->toArray()
                    : [$productRepository->findByUid($uidFilter)]
            )
        );
        return $response;
    }
}